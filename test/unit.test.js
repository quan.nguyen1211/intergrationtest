const chai = require('chai');
const expect = chai.expect;
const { hexToRgb } = require('../src/main');

describe('Hex to RGB Conversion', () => {
  it('should convert a valid hex code to RGB', () => {
    const hex = 'FF5733';
    const result = hexToRgb(hex);
    expect(result).to.deep.equal({ r: 255, g: 87, b: 51 });
  });

  it('should handle a valid hex code with a # symbol', () => {
    const hex = '#00FF00';
    const result = hexToRgb(hex);
    expect(result).to.deep.equal({ r: 0, g: 255, b: 0 });
  });

  it('should return null for an invalid hex code', () => {
    const hex = 'invalid';
    const result = hexToRgb(hex);
    expect(result).to.equal(null);
  });
});
