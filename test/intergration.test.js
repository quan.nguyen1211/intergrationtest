const chai = require('chai');
const expect = chai.expect;
const request = require('supertest');
const app = require('../src/main');

describe('Integration Test', () => {
  it('should convert hex to RGB via API', (done) => {
    request(app)
      .get('/hex-to-rgb/FF5733')
      .end((err, res) => {
        expect(res.status).to.equal(200);
        expect(res.body).to.deep.equal({ hex: 'FF5733', rgb: { r: 255, g: 87, b: 51 } });
        done();
      });
  });
});
