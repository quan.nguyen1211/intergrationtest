const express = require('express');
const app = express();
const port = 3000;

app.use(express.json());

app.get('/hex-to-rgb/:hex', (req, res) => {
  const rgb = hexToRgb(hex);
  res.json(rgb);
});

const hexToRgb = (hex) => {
  hex = hex.replace(/^#/, '');
  if (/^[0-9A-F]{6}[0-9a-f]{0,2}$/i.test(hex) == false) {
    return null;
  }
  const bigint = parseInt(hex, 16);
  const r = (bigint >> 16) & 255;
  const g = (bigint >> 8) & 255;
  const b = bigint & 255;
  return { r, g, b };
}

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});

module.exports = {app, hexToRgb}; // Export the app for testing